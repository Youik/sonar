# sonar

#### 介绍
测试 通过sonar的report-task.txt 文件获取请求获取扫描状态

#### 软件架构
软件架构说明


#### 安装教程

1.  采用docker安装sonarqube

    docker run -d -p 5432:5432 -e POSTGRES_USER=sonar -e POSTGRES_PASSWORD=sonar -e POSTGRES_DB=sonar -e TZ=Asia/Shanghai postgres:10
    docker run -d -p 9000:9000 -e "SONARQUBE_JDBC_URL=jdbc:postgresql://xxx:5432/sonar" -e "SONARQUBE_JDBC_USERNAME=sonar" -e "SONARQUBE_JDBC_PASSWORD=sonar" --name sonarqube sonarqube:8.9.1-community
    打开浏览器: http://ip:9000
    配置一个项目

2.  安装sonar-scanner-cli

    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip
    unzip sonar-scanner-cli-4.6.2.2472-linux.zip
    mv sonar-scanner-4.6.2.2472-linux /usr/local/
    ln -s /usr/local/sonar-scanner-4.6.2.2472-linux/bin/sonar-scanner /usr/local/bin/
    ln -s /usr/local/sonar-scanner-4.6.2.2472-linux/bin/sonar-scanner-debug /usr/local/bin/
    

#### 使用说明
1. 在代码库总执行下列命令

   sonar-scanner   -Dsonar.projectKey=test   -Dsonar.sources=.   -Dsonar.host.url=http://192.168.1.218:9000   -Dsonar.login=d2c48b2b5eca1346dccb7f255820bd8cb068eca7
   
2. 就能看到：

    ll .scannerwork
