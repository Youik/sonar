package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/magiconair/properties"
)

func main() {

	type config struct {
		ProjectKey    string	`properties:"projectKey"`
		ServerUrl     string	`properties:"serverUrl"`
		ServerVersion string	`properties:"serverVersion"`
		DashboardUrl  string	`properties:"dashboardUrl"`
		CeTaskId      string	`properties:"ceTaskId"`
		CeTaskUrl     string	`properties:"ceTaskUrl"`
	}

	type Task struct {
		AnalysisID         string `json:"analysisId"`
		ComponentID        string `json:"componentId"`
		ComponentKey       string `json:"componentKey"`
		ComponentName      string `json:"componentName"`
		ComponentQualifier string `json:"componentQualifier"`
		ErrorMessage       string `json:"errorMessage"`
		ErrorStacktrace    string `json:"errorStacktrace"`
		ExecutedAt         string `json:"executedAt"`
		ExecutionTimeMs    int64  `json:"executionTimeMs"`
		HasErrorStacktrace bool   `json:"hasErrorStacktrace"`
		HasScannerContext  bool   `json:"hasScannerContext"`
		ID                 string `json:"id"`
		Logs               bool   `json:"logs"`
		Organization       string `json:"organization"`
		ScannerContext     string `json:"scannerContext"`
		StartedAt          string `json:"startedAt"`
		Status             string `json:"status"`
		SubmittedAt        string `json:"submittedAt"`
		Type               string `json:"type"`
	}

	type Data struct {
		Task Task `json:"task"`
	}

	p := properties.MustLoadFile("./.scannerwork/report-task.txt", properties.UTF8)

	var c config
	if err := p.Decode(&c); err != nil {
		panic(err)
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET",c.CeTaskUrl, nil)
	if err != nil {
		panic(err)
	}
	token := "d2c48b2b5eca1346dccb7f255820bd8cb068eca7"
	tokenBE := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:", token)))
	req.Header.Add("Authorization", "Basic "+ tokenBE)
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	buff, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	var myData = Data{}
	if err := json.Unmarshal(buff, &myData); err != nil {
		panic(err)
	}
	fmt.Println(myData.Task.Status)
}

